#
# Be sure to run `pod lib lint OpenCV2-framework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'OpenCV2-framework'
  s.version          = '3.4.19'
  s.platform         = :ios, 12.0
  s.summary          = 'OpenCV-framework for iOS.'
  s.description      = <<-DESC
Use the OpenCV2-framework as pod dependency for iOS projects.
                       DESC
  s.homepage         = 'https://gitlab.com/bytepoets/pod-opencv'
  s.license          = { :type => '3-clause BSD License', :file => 'LICENSE' }
  s.author           = { 'Markus Friedl' => 'markus.friedl@bytepoets.com' }
  s.source           = { :git => 'https://gitlab.com/bytepoets/pod-opencv.git', :tag => s.version.to_s }
  s.source_files     = 'opencv2.framework/**'
  s.vendored_frameworks = 'opencv2.framework'

  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
